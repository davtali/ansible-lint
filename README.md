![](https://gitlab.com/davtali/ansible-lint/raw/main/badge/ansible-lint_0.svg)  ![](https://gitlab.com/davtali/ansible-lint/raw/main/badge/ansible-lint_1.svg)  ![](https://gitlab.com/davtali/ansible-lint/raw/main/badge/ansible-lint_2.svg)\
Was tested with ansible-lint 5.1.2 using ansible 2.9.9 for gitlab.com and Gitlab Community Edition 12.2.0
The image badge was host on project, they can work without external library, artefact and on offline.

The project add badge for you gitlab project. \
The color of badge depends on the result lint. \
The lint give your the best bratice for project who use Ansible. \
All project was evaluate for lint \
The badge redirect to the last job where we can see the result lint on terminal \


you can show example on the project : [here](https://gitlab.com/davtali/ansible-mongodb)

for show an alternative version to add badge ansible-lint: [here](https://gitlab.com/davtali/ansible-lint_alt)

For add badge yamllint here [here](https://gitlab.com/davtali/yamllint)


# Quick start

You must create token with scope api on gitlab on your user Setting Account (`user_settings>acces_tokens`). \
Copy your secret token, create variable (`your_project>settings>CICD>variables`) with key=TOKEN and past your token on VALUE 

Add this instruction in your .gitlab-ci.yml
```
stages:
    - lint

ansible-lint:
  stage: lint
  image:
    name: dal3ap1/ansible-lint:latest
    entrypoint: [""]
  script:
    - /project/config/start.sh
```

You can specify the rules who you want ignore it on lint \

```
stages:
    - lint

ansible-lint:
  stage: lint
  image:
    name: dal3ap1/ansible-lint:latest
    entrypoint: [""]
  variables:
    exclude: "meta-video-links,deprecated-module"
  script:
    - /project/config/start.sh
```

for check all the rules show documentation or do `ansible-lint -L`


# Self host

You must overload the BASE_PROJECT_BADGE.

If you rebuild image, you can overload directly on the Dockefile. \
It is the path before your location ansible-lint. (https://gitlab.com/davtali if project was https://gitlab.com/davtali/ansible-lint)
```
ENV BASE_PROJECT_BADGE "https://gitlab.com/username/my_groups/my_subgroup"
# ENV BASE_PROJECT_BADGE "https://gitlab.com/davtali"
```
`docker build -t ansible-lint .`

Or you can overload the BASE_PROJECT_BADGE on the CI.
```
stages:
    - lint

ansible-lint:
  stage: lint
  image:
    name: dal3ap1/ansible-lint:latest
    entrypoint: [""]
  variable:
    exclude: "meta-video-links,deprecated-module"
    BASE_PROJECT_BADGE: ""https://gitlab.com/username/my_groups/my_subgroup"
    #BASE_PROJECT_BADGE: "https://gitlab.com/davtali"
  script:
    - /project/config/start.sh
```


# Use without ci (and badge)

For use only the lint part:
```
docker run --rm -v $PWD:/project/playbook dal3ap1/ansible-lint:latest
```
You can specify the rules who you want ignore it on lint 
```
docker run --rm -v $PWD:/project/playbook -e exclude=risky-file-permissions,no-changed-when dal3ap1/ansible-lint:latest
```

# deployment for many project

The script iterate all project (and groups) from root to leaf \
They can add, edit, delete job .gitlab-ci.yml on specifiq branch. \
Can add or delete badge \
You must have the TOKEN on the root project. \

gitlab-runner id
detect .yaml
