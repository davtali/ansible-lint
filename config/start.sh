#!/bin/sh

ansible-lint -f rich --force-color -x $exclude . 1 > err.txt 2>output.txt


tmp=$(cat output.txt | grep "Finished with ")
failure=$(cat output.txt| grep "Finished with " | sed -e 's/.*with \(.*\)failure.*/\1/' | sed "s,\x1B\[[0-9;]*[a-zA-Z],,g");
failure=${failure:-0}

warning=$(cat output.txt | grep "Finished with " | sed -e 's/.*, \(.*\)warning.*/\1/' | sed "s,\x1B\[[0-9;]*[a-zA-Z],,g");
warning=${warning:-0}

if (( $failure+$warning==0 )); then
state="0"

elif (( $failure>0 )); then
state="1"

else
state="3"
fi


# print on runner
cat err.txt
if [[ $failure+$warning -gt 0 ]];then
echo $tmp
fi


# ---------------- if the use is for gitlab ci
if ! ([ -z "$TOKEN" ] || [ -z "$CI_JOB_URL" ] || [ -z "$CI_SERVER_HOST" ] || [ -z "$CI_API_V4_URL" ] || [ -z "$CI_PROJECT_ID" ])
then


# rm -rf .cache;
#var=$( echo $CI_PROJECT_NAMESPACE |  awk -F/ '{print $1}')
#if [ "$var" != "ansible" ]; then
# init badge if the project was not on group ansible, but have folder playbook
#var=$(find -type d | grep -E "/playbook$" | wc -l)
#if [ $var -gt 0 ]; then
#/project/config/function.py init_badge img_url
#fi
# init badge if project was on groupe ansible
#else
#/project/config/function.py init_badge img_url
#fi


#img_url="https://${CI_SERVER_HOST}/${GITLAB_USER_LOGIN}/ansible-lint/raw/main/badge/ansible-lint_${etat}.svg"
img_url="${BASE_PROJECT_BADGE:-"https://gitlab.com/davtali"}/ansible-lint/raw/main/badge/ansible-lint_"
# init_badge for all project
/project/config/function.py init_badge $img_url



id=$(/project/config/function.py get_id 2>&1 > /dev/null);
# redirect print to null for send id by stderr, for don't show this output on pipeline ci

# -v
curl --silent --output /dev/null --request PUT --header "PRIVATE-TOKEN: $TOKEN" \
-d "link_url=${CI_JOB_URL}" \
-d "image_url=${img_url}${state}.svg" \
${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/badges/${id};
fi
