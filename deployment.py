#!/usr/bin/python3
import requests
import json
import gitlab
import subprocess
import docker
import os
import re
import datetime



racine=159

base='https://my-gitlab.com/'
header={"PRIVATE-TOKEN": TOKEN}
gl=gitlab.Gitlab(base, private_token=TOKEN)



def main():
    # recupere tout les id fils du projet 49
    # ids=getIDs(racine)
    ids=[223]

    # ajoute job ou modifie job ou supprime job sur les fichier ci (uniquement sur les branches de type dev)
    gitlab_ci(ids)

    # del les badges
    # delete_badge(ids)

    # donne des info
    info(ids)




    ########################## changement de strategie

    # initilise les badges yamllint et ansible-lint a touts les projet 
    # init_badge(ids)

    # selectionne les id avec la branch develop
    # id_develop=separe_develop(ids)[0]

    # selectionne les id avec au moins une branch de type dev
    # id_develop=separe_develop(ids)[0]

    # separe les id de projet avec fichier gitlab ou non
    # id_gitlab,id_pas_gitlab=separe_id_develop(id_develop)

    # upload le fichier ci pour les projet qui n'en possede pas
    # upload_ci(id_pas_gitlab)

    # update le fichier ci pour les projet qui en possede
    # maj_ci(id_gitlab)
    ########################## changement de strategie







############################################# utilsation
###################################
########################


# le script comporte des fonctions pour ajouter un job (s il n est pas deja present), et pour modifier un job (s il est present)




# prochaine maj gitlab ==> changer le parcours des projets

# gitlab >=13.5


############################################# details technique
###################################
########################

# l edition du fichier ci se fait dans un contener via l outils yq qui permet de modifier des fichiers .yml
# cependant lorsque yq edite un paragraphe, il peut modifier le formatage de certaine autre localisation du meme fichier voir meme ajouter des caracteres.
# ce type d adaption se trouve dans les fonction "fleche_to_l2" et "l2_to_fleche" que nous detaillons ci dessous




# lors de l execution du ci dans une pipeline, il est necessaire de faire la difference entre "|" et "-" et "- >"
# yq respect le formatage pour multiligne avec "|" et monoligne "-"
# Or avec le  "- >" il va se comporter comme avec du "-" et donc faire du monoligne

# Donc Pour gerer les "- >" qui represente les multi ligne sur on les fait passer sur yq en tant que "|-"

# Note:
# pour ne pas avoir de conflit on differencie lors des reconvertions, on differencie "- | " et "- |-"




# dans les exemples suivant extraits de fichier gitlab-ci.yml trouvé sur le git, on fait des convertions pour appliquer le yq
# dans le cas 1, on fait la convertion, applique yq, puis la reconvertion sans faire ajustement
# dans le cas 2, on fait la convertion, applique yq, puis la reconvertion en ajustant soit meme le formatage |2- 

####################### EXEMPLE 1: lorsque le script comporte des - > ( peu frequent)

# ETAPE1 :
# recuperation du code depuis gitlab-ci.

#   script:
#     # - docker login -u ${REGISTRY_USER} -p ${REGISTRY_PWD} ${REGISTRY_URL}
#     - ansible-galaxy install -r tests/requirements.yml
#     # Run the first time
#     - ansible-playbook tests/test.yml -i tests/inventory -c local -b
#     # Run agin and expect no changes
#     - >
#       ansible-playbook tests/test.yml -i tests/inventory -c local -b
#       | grep -q 'changed=0.*failed=0'
#       && (echo 'Idempotence test: pass' && exit 0)
#       || (echo 'Idempotence test: fail' && exit 1)
#   when: manual


# ETAPE 2:
# convertion de "- >" a "- |-" 

#   script:
#     # - docker login -u ${REGISTRY_USER} -p ${REGISTRY_PWD} ${REGISTRY_URL}
#     - ansible-galaxy install -r tests/requirements.yml
#     # Run the first time
#     - ansible-playbook tests/test.yml -i tests/inventory -c local -b
#     # Run agin and expect no changes

#     - |-
#       ansible-playbook tests/test.yml -i tests/inventory -c local -b
#       | grep -q 'changed=0.*failed=0'
#       && (echo 'Idempotence test: pass' && exit 0)
#       || (echo 'Idempotence test: fail' && exit 1)
#   when: manual


# ETAPE 3:
# utiliastion de yq, et recuperation de la sortie
# rien est modifié


# ETAPE 4:
# convertion de "- |-" a "- >"

#   script:
#     # - docker login -u ${REGISTRY_USER} -p ${REGISTRY_PWD} ${REGISTRY_URL}
#     - ansible-galaxy install -r tests/requirements.yml
#     # Run the first time
#     - ansible-playbook tests/test.yml -i tests/inventory -c local -b
#     # Run agin and expect no changes

#     - >
#       ansible-playbook tests/test.yml -i tests/inventory -c local -b
#       | grep -q 'changed=0.*failed=0'
#       && (echo 'Idempotence test: pass' && exit 0)
#       || (echo 'Idempotence test: fail' && exit 1)
#   when: manual





####################### EXEMPLE 2: lorsque le script commence par un - > (cas très frequent)


# ETAPE 1:
# recuperation du code depuis gitlab-ci.

#   script:
#     - >
#       docker run
#       --rm
#       -v $PWD:/project/playbook
#       -e CI_API_V4_URL=${CI_API_V4_URL}
#       -e CI_PROJECT_ID=${CI_PROJECT_ID}
#       -e CI_PROJECT_URL=${CI_PROJECT}
#       -e CI_SERVER_HOST=${CI_SERVER_HOST}
#       -e CI_JOB_URL=${CI_JOB_URL}
#       -e CI_JOB_NAME=${CI_JOB_NAME}
#       -e TOKEN=${TOKEN}
#       -e exclude="
#       line-length: disable"
#       dal3ap1/yamllint:latest;
#       docker rmi dal3ap1/yamllint:latest;

# ETAPE 2:
# convertion de "- >" a "- |-" 

#   script:
#     - |-
#       docker pull dal3ap1/ansible-lint:latest;
#       docker run
#       --rm
#       -v $PWD:/project/playbook
#       -e CI_API_V4_URL=${CI_API_V4_URL}
#       -e CI_PROJECT_ID=${CI_PROJECT_ID}
#       -e CI_PROJECT_URL=${CI_PROJECT}
#       -e CI_SERVER_HOST=${CI_SERVER_HOST}
#       -e CI_JOB_URL=${CI_JOB_URL}
#       -e CI_JOB_NAME=${CI_JOB_NAME}
#       -e TOKEN=${TOKEN}
#       -e exclude="no-changed-when,line-length,experimental"



# ETAPE 3:
# utiliastion de yq, et recuperation de la sortie
# la sortie a été modifié par yq

#   script: |2-
#           docker run
#           --rm
#           -v $PWD:/project/playbook
#           -e CI_API_V4_URL=${CI_API_V4_URL}
#           -e CI_PROJECT_ID=${CI_PROJECT_ID}
#           -e CI_PROJECT_URL=${CI_PROJECT}
#           -e CI_SERVER_HOST=${CI_SERVER_HOST}
#           -e CI_JOB_URL=${CI_JOB_URL}
#           -e CI_JOB_NAME=${CI_JOB_NAME}
#           -e TOKEN=${TOKEN}
#           -e exclude="
#           line-length: disable"




# ETAPE 4:
# convertion de "|2-" a "- >"
# on gere le decalage et le passage en - >

#   script:
#     - >
#       docker run
#       --rm
#       -v $PWD:/project/playbook
#       -e CI_API_V4_URL=${CI_API_V4_URL}
#       -e CI_PROJECT_ID=${CI_PROJECT_ID}
#       -e CI_PROJECT_URL=${CI_PROJECT}
#       -e CI_SERVER_HOST=${CI_SERVER_HOST}
#       -e CI_JOB_URL=${CI_JOB_URL}
#       -e CI_JOB_NAME=${CI_JOB_NAME}
#       -e TOKEN=${TOKEN}
#       -e exclude="no-changed-when,line-length,experimental"
#       dal3ap1/ansible-lint:latest;
#       docker rmi dal3ap1/ansible-lint:latest;



def getIDs(racine):
    '''
    get tout les id des projets ascendant
    Si le projet ne possede pas de fils, return l'id du projet racine
    -----------------------
    --input
    racine: int id du groupe
    --ouput
    res: int list
    '''


    def final(id):
        '''
        get les id des projet du path courant
        '''
        url="base"+"/api/v4/groups/"+str(id)
        r=requests.get(url).json()
        res=[]

        for ele in r["projects"]:
            res.append(ele["id"])
            
        return res


    def decoupe(id):
        '''
        get id des sous projet du path courant
        '''
        url=base+"api/v4/groups/"+str(id)+"/subgroups"

        r=requests.get(url).json()
        res=[]
        for dico in r:
            res.append(dico["id"])
        return res

    # on verifie si la racine du projet possede des fils, sinon on s'arrete
    url=base+"api/v4/groups/"+str(racine)
    r=requests.get(url).json()
    if "projects" not in r: return [racine]
    
    # s'il possede des fils, on les recuperes
    stack=[racine]
    res=[] # continent les id des projets
    cherche=1
    while cherche==1:
        if stack==[]:
            cherche=0
            break

        petit=final(stack[0])
        grand=decoupe(stack[0])
        stack=stack+grand
        del stack[0]
        # amelioration - index au lieu de del
        res=res+petit
        # amelioration - add subliste ald merge
    return res




# res=getIDs(racine)
def init_badge(ids):
    '''
    initilisise les badges sur gitlab
    ---------------------------
    input
    res: int list des id projet
    '''

    # parcours des id projet
    for i in ids:

        url=base+"api/v4/projects/"+str(i)+"/badges"
        rep=requests.get(url=url, headers=header).json()

        # on regarde si le projet possede deja un badge de type "ansible-lint"
        badge_ansible=0
        badge_yamllint=0
        for badge in rep:
            tmp=badge['image_url']
            select=tmp[tmp.rindex('/')+1:tmp.rindex('_')]
            if select=="ansible-lint": badge_ansible=1
            if select=="yamllint": badge_yamllint=1


        # s'il en a pas le badge_ansible, on l'ajoute
        if not badge_ansible:
            dico={"image_url":base+"Docker/lint/ansible_lint/raw/master/badge/ansible-lint_2.svg",
                "link_url":base
            }
            requests.post(url=url, headers=header, data=dico)

        
        if not badge_yamllint:
            dico={"image_url":base+"Docker/lint/yamllint/raw/master/badge/yamllint_2.svg",
                "link_url":base
            }
            requests.post(url=url, headers=header, data=dico)


def delete_badge(ids):
    '''
    supprime les badges ansible-lint et yamllint des projets
    ---------------------------
    input
    ids: int list des id projet
    '''


    for i in ids:

        url=base+api/v4/projects/"+str(i)+"/badges"
        rep=requests.get(url=url, headers=header).json()

        id_badge=[]
        for badge in rep:
            tmp=badge['image_url']
            select=tmp[tmp.rindex('/')+1:tmp.rindex('_')]
            if select=="ansible-lint" or select=="yamllint": id_badge.append(badge['id'])
            

        for bdg in id_badge:
            requests.delete(url=url+"/"+str(bdg), headers=header)



def separe_develop(ids):
    '''
    separe les id de projet en deux liste, celle qui possede la branche develop et les autres
    ----input
    ids: liste int des id des projets
    ----ouput
    id_develop: list int des id avec branch develop
    id_pas_develop: list int des autres
    '''
    id_pas_develop=[]
    id_develop=[]
    for iid in ids:
        url=base+api/v4/projects/"+str(iid)+"/repository/branches"
        rep=requests.get(url=url, headers=header).json()
        present=0
        
        for branch in rep:
            if branch['name']=="develop":
                    present=1
                    break

        id_develop.append(iid) if present else id_pas_develop.append(iid)
    return id_develop,id_pas_develop


def update_yamllint_v1():
    '''
    modifie l'interieur du job yamllint
    '''

    rep=subprocess.run('''docker exec -it kk yq eval -i '
    .yamllint.tags=["lint"] |
    .yamllint.script="
    env | grep CI_ > tmp;
    docker run
    --rm
    --env-file tmp
    -v $PWD:/project/playbook
    -e TOKEN=${TOKEN}
    -e exclude=\\"
    line-length: disable\\"
    dal3ap1/yamllint:latest;
    docker rmi dal3ap1/yamllint:latest"
    ' .gitlab-ci.yml
    ''',
    shell=True,
    text=True,
    stdout=subprocess.PIPE
    )


# la v4 de mike yq ne permet pas d effacer de key, on contourne autrement (possibilite de utiliser la v3)
def delete_job(key):
    '''
    efface le job
    '''
    
    if key=="ansible-lint":
        rep=subprocess.run('''docker exec -it kk yq e -i '
        .ansible-lint="cocovide19"
        ' .gitlab-ci.yml
        ''',
        shell=True,
        text=True,
        stdout=subprocess.PIPE
        )

        f=open(".gitlab-ci.yml","r+")
        txt=f.read()
        txt=txt.replace('''ansible-lint: cocovide19\n''',"")
        f.seek(0)
        f.truncate(0)
        f.write(txt)
        f.close()



    if key=="yamllint":
        rep=subprocess.run('''docker exec -it kk yq eval -i '
        .yamllint="cocovide19"
        ' .gitlab-ci.yml
        ''',
        shell=True,
        text=True,
        stdout=subprocess.PIPE
        )

        f=open(".gitlab-ci.yml","r+")
        txt=f.read()
        txt=txt.replace('''yamllint: cocovide19\n''',"")
        f.seek(0) # place curseur
        f.truncate(0)
        f.write(txt)
        f.close()





def update_ci(proj,path,br,target=["ansible-lint","yamllint"]):
    '''
    permet editer le fichier gitlab-ci.yml
    (de)commenter certaine fonction, il est possible de:
    - ajouter des jobs
    - mettre a jours des jobs
    - supprimer des jobs
    '''
    f=proj.files.get(".gitlab-ci.yml",ref=br)
    res=f.decode().decode("utf-8")
    fleche_to_l2(res)
    stage=get_stage()

    add=0
    update=0

    if "lint" not in stage: add_stage("lint",stage)

    # on parcours nos target, et on applique une action 
    # add_job - delete_job - update-job
    # en fonction de la presence de la cle sur le fichier .gitlab-ci.yml

    for tg in target:
        if not check(tg):
            pass
            add_job(tg)
            add=1
        else:
            continue
            ## update_job(tg)
            delete_job(tg)
            update=1

    # si y a eu un ajout on fait la mise en forme et on envoie le commit
    if add:
        f.content=l2_to_fleche()
        # git push
        f.save(branch=br, commit_message="ajout de job lint")
        line_prepender(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S  [ADD_JOB]  "+br+"  "+path))

    if update:
        f.content=l2_to_fleche()
        # f.save(branch=br, commit_message="modification de job lint")
        f.save(branch=br, commit_message="supp des jobs lint")
        line_prepender(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S  [MODIFY_JOB]  "+br+"  "+path))

    if not add and not update: line_prepender(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S  [ALREADY_UPDATED]  "+br+"  "+path))





# a supp
def separe_id_develop(id_develop):
    '''
    separe la liste des id, en deux, celle qui corresponde au projet avec un fichier .gitlab-ci.yml et les autres
    ----input
    id_develop: list int
    ----ouput
    '''
    id_gitlab=[]
    id_pas_gitlab=[]
    for iid in id_develop:
        url=base+"api/v4/projects/"+str(iid)+"/repository/files/.gitlab-ci.yml/raw?ref=develop"


        id_gitlab.append(iid) if requests.get(url=url, headers=header).status_code==200 else id_pas_gitlab.append(iid)

    return id_gitlab,id_pas_gitlab

def line_prepender(line):
    if not os.path.exists("log.txt"): open("log.txt",'w').close()
    with open("log.txt", 'r+') as f:
        content=f.read()
        f.seek(0,0)
        f.write(line.rstrip('\r\n')+"\n"+content)
    print(line)






def upload_ci(proj,path,br,target=["ansible-lint","yamllint"]):
    '''
    upload le fichier .gitlab-ci.yml dans le projet id et la branche br
    ----input
    idd: int des id projet
    br: str branche id
    '''

    # with open('./gitlab-ci.default', 'r') as my_file:
    #     file_content = my_file.read()
    # print("a lire")

    # f=open(".gitlab-ci.yml","w")
    # f.write("stages:\n- lint\n")
    # f.close()

    with open(".gitlab-ci.yml","w") as f:
        f.write("stages:\n- lint\n")

    for tg in target:
        add_job(tg)
    
    proj.files.create({'file_path': '.gitlab-ci.yml',
                                'branch': br,
                                'content': l2_to_fleche(),
                                'commit_message': 'ajout de ci'})
    
    line_prepender(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S  [UPLOAD]  "+br+"  "+path))



def separe_id_develop(id_develop,br="develop"):
    '''
    separe la liste des id, en deux, celle qui corresponde au projet avec un fichier .gitlab-ci.yml et les autres
    ----input
    id_develop: list int
    ----ouput
    '''
    id_gitlab=[]
    id_pas_gitlab=[]
    for iid in id_develop:
        url=base+"api/v4/projects/"+str(iid)+"/repository/files/.gitlab-ci.yml/raw?ref="+br

        id_gitlab.append(iid) if requests.get(url=url, headers=header).status_code==200 else id_pas_gitlab.append(iid)

    return id_gitlab,id_pas_gitlab

def check(key):
    '''
    regarde si la key est presente
    '''
    rq='''docker exec -it kk yq eval --no-colors '.'''+key+'''' .gitlab-ci.yml'''
    rep=subprocess.run(rq,
    shell=True,
    stdout=subprocess.PIPE,
    text=True)
    
    if rep.stdout=="null\n" : return False
    return True

def fleche_to_l2(txt):
        f=open(".gitlab-ci.yml","w")
        # ancien
        # ordre est important, permet de changer les "- >" d'entete de script en "|2-" 
        # et les "- >" en instructions de script en "|-"
        # txt=txt.replace(":\n    - >",": |2-").replace("\n    - >","\n    - |-")
        
        # nouveau
        txt=txt.replace("\n    - >","\n    - |-")

        f.write(txt)
        f.close()



def l2_to_fleche():
        f=open(".gitlab-ci.yml","r")
        txt=f.read()

        # ancien
        # pour passer du format "|2-" a "- >" on supprime le "|2-" pour mettre a la ligne du dessous "- >"
        # or cette operation, laisse un alinea large de 12espaces, au lieu de 6, donc on rectifie pour avoir un alignement parfait
        # txt=txt.replace(" |2-","\n\t- >".expandtabs(4)).replace("\n            ","\n      ").replace("\n    - |-","\n    - >")

        # nouveau
        # txt=txt.replace("\n            ","\n      ").replace("\n    - |-","\n    - >").replace(": |2-",":\n    - >")
        txt=txt.replace("\n        ","\n      ").replace("\n    - |-","\n    - >").replace(": |2-",":\n    - >")


        # permet d'ajouter un saut de ligne avant chaque job
        pattern="\n([a-z]\S*?):\n"
        st=re.findall(pattern,txt)
        for ele in st:
            txt=txt.replace("\n"+ele+":","\n\n"+ele+":")
        print(txt)
        return txt




def add_job(key):
    '''
    permet d'ajouter un job en modifiant le fichier .gitlab-ci.yml
    '''
    if key=="ansible-lint":

        rep=subprocess.run('''docker exec -it kk yq e -i '
        .ansible-lint.tags=["lint"] |
        .ansible-lint.stage="lint" |
        .ansible-lint.script="
    env | grep CI_ > tmp;
    docker run
    --rm
    --env-file tmp
    -v $PWD:/project/playbook
    -e TOKEN=${TOKEN}
    -e exclude=\\"no-changed-when,line-length,experimental\\"
    dal3ap1/ansible-lint:latest;
    docker rmi dal3ap1/ansible-lint:latest;" 
        ' .gitlab-ci.yml
        ''',
        shell=True,
        text=True,
        stdout=subprocess.PIPE
        )

    
    if key=="yamllint":

        rep=subprocess.run('''docker exec -it kk yq eval -P -i '
        .yamllint.tags=["lint"] |
        .yamllint.stage="lint" |
        .yamllint.script="
    env | grep CI_ > tmp;
    docker run
    --rm
    --env-file tmp
    -v $PWD:/project/playbook
    -e TOKEN=${TOKEN}
    -e exclude=\\"
    line-length: disable\\"
    dal3ap1/yamllint:latest;
    docker rmi dal3ap1/yamllint:latest;"
        ' .gitlab-ci.yml
        ''',
        shell=True,
        text=True,
        stdout=subprocess.PIPE
        )








def add_stage(key,stage):
    '''
    ajoute la key au debut de stage
    -----------
    --input
    key: str
    stage: str list qui contient tout les key stages
    '''

    # stage=["aaaa", "bbbbb","autre", "salut"]
    stage.insert(0,key)
    tmp='","'.join(stage)
    res='''["'''+tmp+'''"]'''
    
    # rq='''docker exec -it kk yq eval --no-colors -i '."stages"=["aaaa", "bbbbb"]' .gitlab-ci.yml '''
    rq='''docker exec -it kk yq eval --no-colors -i -P '."stages"='''+res+''' ' .gitlab-ci.yml '''

    rep=subprocess.run(rq,
    shell=True,
    text=True,
    stdout=subprocess.PIPE
    )



def get_stage():
        '''
        get stage
        -----------
        ouput: list str qui contient les stages actuels
        '''
        rep=subprocess.run('''docker exec -it kk yq eval '.stages.[] | select(. == "*") ' .gitlab-ci.yml''',
        shell=True,
        stdout=subprocess.PIPE,
        text=True)

        # la sortie cli ajoute un \n en derniere ligne
        stage=rep.stdout.split("\n")[:-1]
        return stage



def getall_dev_branch(proj):
        # recupere tout les branches du projet, et garde seulement les projets contenant "dev" dans le nom de la branche
        name_br=[]

        for ele in proj.branches.list():name_br.append(ele.name)

        r=re.compile("\S*dev", re.IGNORECASE)
        dev_br=list(filter(r.match,name_br))
        return dev_br




def gitlab_ci(id_gitlab):

    subprocess.run('''docker run -it -d --rm --name kk --entrypoint /bin/sh -v $PWD:/workdir mikefarah/yq''',
    shell=True,
    stdout=subprocess.PIPE)
    
    
    for idd in id_gitlab:

        url=base+"/api/v4/projects/"+str(idd)
        rep=requests.get(url=url, headers=header).json()
        proj=gl.projects.get(idd)
        

        # parcours de branches
        for br in getall_dev_branch(proj):
            # print("--------",br)

            # activation du runner 5 : pic-nexus
            # try:
            #     proj.runners.create({'runner_id':5})
            # except gitlab.exceptions.GitlabCreateError as err:
            #     pass

            # activation du runner 50 : lint
            try:
                proj.runners.create({'runner_id':50})
            except gitlab.exceptions.GitlabCreateError as err:
                pass

            # non recurcif
            # items=proj.repository_tree(ref=br)
            # for ele in items:
            #     print(ele)

            

            url=base+"/api/v4/projects/"+str(idd)+"/repository/tree"
            dico={
                "path":"/",
                "ref":br,
                "recursive":"True",
                "per_page":"100"
            }

            items=requests.get(url=url, headers=header, data=dico).json()
            
            # ----- recherche de fichier et de dossier dans le repo
            ci=0
            yaml=0
            playbook=0
            for dico in items:
                n=len(dico['name'])

                # si fichier '.gitlab-ci.yml'
                if dico['type']=="blob" and dico['name']==".gitlab-ci.yml" and dico["path"]==".gitlab-ci.yml":
                    ci=1
                    yaml=1
                
                # si dossier 'playbook'
                if dico['type']=="tree" and dico['name']=="playbook": playbook=1

                # si le nom fichier finis par '.yml'
                if dico['type']=="blob" and dico['name'][n-4:n]==".yml":yaml=1
                
                if ci and playbook: break
                

            # ----- si pas de fichier ci, on en creer un
            if not ci:
                # mais on doit gerer si le fichier doit comporter tout les jobs

                # si le groupe est ansible, OU, si y a un playbook dans le repo
                if (rep['path_with_namespace'].split("/")[0] == "ansible" or playbook):
                    upload_ci(proj,rep['path_with_namespace'],br)

                # sinon si y a du .yml on mets au moins 1 badge
                elif yaml:
                    upload_ci(proj,rep['path_with_namespace'],br,target=["yamllint"])
                # on ne veut pas de job ansible-lint dans un projet qui n'est ni dans un groupe ansible
                # et ni ne contient de dossier playbook


            # ----- si deja fichier ci, on en creer pas un, (on editera sous condition)


            # on possede toujours les meme contraintes que precedement
            elif (rep['path_with_namespace'].split("/")[0] == "ansible" or playbook):
                update_ci(proj,rep['path_with_namespace'],br)


            elif yaml:
                update_ci(proj,rep['path_with_namespace'],br,target=["yamllint"])



    # rm container
    subprocess.run('''docker kill kk''',
    shell=True,
    text=True,
    stdout=subprocess.PIPE
    )


def info(ids):
    '''
    Permet d'obtenir les info sur les badges
    vert = yaml_vert + ansible_vert
    rouge = yaml_rouge + ansible_rouge
    n = nbr de projet

    objectif:
    detecter les projet avec au moins 1 rouge
    '''
    
    yaml_vert=[]
    yaml_rouge=[]
    ansible_vert=[]
    ansible_rouge=[]

    for idd in ids:
        # if idd!=57: continue
        
        url=base+"api/v4/projects/"+str(idd)
        path=requests.get(url=url, headers=header).json()['path_with_namespace']


        url=base+"api/v4/projects/"+str(idd)+"/badges"
        badges=requests.get(url=url, headers=header).json()


        for badge in badges:
            tmp=badge['image_url']
            select=tmp[tmp.rindex('/')+1:tmp.rindex('.')]
            if select=="ansible-lint_0": ansible_vert.append(path)
            elif select=="ansible-lint_1": ansible_rouge.append(path)
            elif select=="yamllint_0": yaml_vert.append(path)
            elif select=="yamllint_1": yaml_rouge.append(path)
        

    print("\n\t\tansiblle_vert\n",ansible_vert)
    print("\n\t\tansiblle_rouge\n",ansible_rouge)
    print("\n\t\tyamllint_vert\n",yaml_vert)
    print("\n\t\tyamllint_rouge\n",yaml_rouge)
    print("\n--------------------------------------------------------")
    print("\n\nnombre de projet visite: ",len(ids))    
    print("\n\nansible:\t\tyamllint:")
    print("vert: ",len(ansible_vert),"\t\tvert:",len(yaml_vert))
    print("rouge: ",len(ansible_rouge),"\t\trouge:",len(yaml_rouge))
    print("\n")

        
    
if __name__=="__main__":
    main()

